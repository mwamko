#    Mwamko  Copyright (C) 2007  Aleksandr O. Levchuk
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Constants of Mwamko are defined here. This includes:
#  * Paths to External Configuration
#  * RAILS_ROOT
#  * WHOAMI
#  * Dependencies
#  * Network Configuration
#  * Safety/Strength contracts
#  * Error messages

# Mwamko has to be restarted when this file is changed
# Run: ./script/mwamko restart

# Path to External Configuration
EXTERNAL_CONFIGURATION_DIR = '/etc/mwamko'
ALLOWED_EMAILS_FILE = "#{EXTERNAL_CONFIGURATION_DIR}/allowed_emails.list"
require "#{EXTERNAL_CONFIGURATION_DIR}/available_ip_addresses.range.rb"
require "#{EXTERNAL_CONFIGURATION_DIR}/available_local_ip_addresses.range.rb"
require "#{EXTERNAL_CONFIGURATION_DIR}/dns.settings.rb"

# The directory path and the file name of this script
module MwamkoConstants
  SCRIPT_DIR = File.dirname(File.expand_path(__FILE__))
  SCRIPT_NAME = $0.split('/')[-1]
end

# The path to all files of Mwamko
RAILS_ROOT = File.expand_path("#{MwamkoConstants::SCRIPT_DIR}/..")

# The user that is running Mwamko
WHOAMI = `whoami`.chomp

# On the remote server. These strings will be piped through ssh.
REMOTE_VZTOOLS_PATH  = '/usr/sbin'
REMOTE_SUDO          = '/usr/bin/sudo'

# Dependencies 
GIT          = '/usr/local/bin/git'
RUBY_PATH    = '/usr/bin/env ruby'
SSH          = '/usr/bin/ssh'
SSH_KEYSCAN  = '/usr/bin/ssh-keyscan'
SSH_KEYGEN   = '/usr/bin/ssh-keygen'
TO_SSH       = Proc.new do |cmd, host|
  raise "Double Quotes are not allowed in cmd" if cmd =~ /"/

  if !(host =~ SAFE_DOMAIN) or !(host =~ SAFE_IP4)
    # TODO: Bring this check into the the OpenvzServer model
    raise "Unsafe host: #{host.inspect}"
  end
  
  
  ["echo \"#{cmd}\" | #{SSH}",
   "-T",
   "-o StrictHostKeyChecking=yes",
   "-o UserKnownHostsFile=#{RAILS_ROOT}/tmp/ssh_known_openvz_hosts",
   "-i #{RAILS_ROOT}/tmp/mwamko_id_rsa",
   "#{host}"
  ].join("\\\n ")
end

SUDO         = '/usr/bin/sudo'
TOUCH        = '/usr/bin/touch'
GZIP         = '/bin/gzip'
RM           = '/bin/rm'
KILL         = '/bin/kill'
CAT          = '/bin/cat'
TAIL         = '/usr/bin/tail'
PS           = '/bin/ps'
LIGHTTPD     = '/usr/bin/env lighttpd'
SPAWNER_PATH = '"/usr/bin/env spawn-fcgi"' # comes with Lighttpd

# Methods that may be useful anywhere 
require File.expand_path("#{RAILS_ROOT}/vendor/open4/lib/open4")
def RUN4(cmd)
  output = Struct.new(:exitstatus, :pid, :errors, :out).new
  output[:exitstatus] = Open4::popen4(cmd) do |pid, cmd_in, cmd_out, cmd_err|
    cmd_in.close
    output[:pid    ] = pid
    t1 = Thread.new {output[:errors ] = cmd_err.read}
    t2 = Thread.new {output[:out    ] = cmd_out.read}
    t1.join # A timeout can be specified here
    t2.join # and here
  end

  return output
end

NO_DEFAULT_RAILS_ENV = 
  [
   "NOTICE:",
   "  There is no default Rails Environment for this utility.",
   "",
   "  If the environmental variable RAILS_ENV (of the shell that is",
   "  running the Ruby interpreter) is not set, then an error will be",
   "  raised.",
   "",
   "  Read the error message for instructions on how to set RAILS_ENV.",
  ].join("\n")

if !ENV.has_key? "RAILS_ENV"
  raise "ENV['RAILS_ENV'] is not set!\n" + 
    "If you are in a command line shell, try:\n" +
    "  export RAILS_ENV=development\n" +
    "\n" +
    "If you are in Ruby interpreter, try:\n" +
    "  ENV['RAILS_ENV'] = 'development'\n" +
    "\n"
end


if ![:development, :test, :production].include? ENV['RAILS_ENV'].to_sym
  raise "ENV['RAILS_ENV'] is #{ENV['RAILS_ENV'].inspect}.\n" +
    "That's not a valid Rails Environment.\n"
end

# Network Configuration
#   To learn more run: ./script/process/spawner --help
SPAWNER_PLATFORM = 'fcgi'
RESPAWN_PERIOD   = 100000000 # Repeat spawn attempts every 3 years

#   The following can be configured differently per environment
lighttpd_port                          = {}
lighttpd_port[:development]            = 7000
lighttpd_port[:test]                   = 8000
lighttpd_port[:production]             = 1024

lighttpd_bind                          = {}
lighttpd_bind[:development]            = '0.0.0.0'
lighttpd_bind[:test]                   = '0.0.0.0'
lighttpd_bind[:production]             = '0.0.0.0'

DISPATCHER_URL                         = '/dispatch.fcgi'
DISPATCHER_BIND                        = '0.0.0.0'
dispatcher_starting_port               = {}
dispatcher_starting_port[:development] = 7001
dispatcher_starting_port[:test]        = 8001
dispatcher_starting_port[:production]  = 9001

dispatcher_num_instances               = {}
dispatcher_num_instances[:development] = 5
dispatcher_num_instances[:test]        = 5
dispatcher_num_instances[:production]  = 5

AJAXTERM_BIND                          = '0.0.0.0'
ajaxterm_starting_port                 = {}
ajaxterm_starting_port[:development]   = 7001
ajaxterm_starting_port[:test]          = 8001
ajaxterm_starting_port[:production]    = 9000
# The number of instances will be the total number of available ips
# See: /etc/mwamko/available_ip_addresses.range

# The following code will form Constants out of the development/test/production
# triple local hashes according to the current environment
local_variables.each do |name|
  var = eval(name)
  if var.class == Hash
    finger_print = [var.has_key?(:development),
                    var.has_key?(:test),
                    var.has_key?(:production)]
    if finger_print == [false, false, false] # This is not the Hash the we need
      next
    elsif finger_print == [true, true, true] # This is the one
      new_constant = name.upcase
      eval "#{new_constant}=#{name}[ENV['RAILS_ENV'].to_sym]"
    else
      STDERR.puts "There is possibly a spelling error in the keys of #{name}:"
      STDERR.puts "  #{var.keys.inspect}"
    end

  end
end



# Mwamko is not yet able to manage multiple templates
# that is why this code block returns an array of only ONE element
GET_TEMPLATES    = Proc.new {["debian-4.0-i386-minimal"]}


# Safety contracts
SAFE_PASSWORD    = /\A([a-zA-Z0-9%!@$&*()^# ])+\Z/
SAFE_EMAIL       = /\A[[:alnum:]\+\._]+@[[:alnum:]\-\.]+\Z/
SAFE_DOMAIN      = /\A[[:alnum:]\-\.]+\Z/
SAFE_USERNAME    = /\A[[:alnum:]\._]+\Z/ # Almost like the first part of EMAIL
SAFE_VEID        = /\A[0-9]+\Z/
SAFE_UID         = /\A[0-9]+\Z/
SAFE_IP4         = /\A\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\Z/
SAFE_NETIF_NAME  = /\A[a-z0-9]+\Z/
SAFE_PID         = /\A\d+\Z/


# Strength contacts
def PASSWORD_IS_STRONG(password)

  # Not a Word
  extract_alpha = password.scan(/[[:alpha:]]/).join('')
  cmd = "grep -i \"^#{extract_alpha}$\" #{MwamkoConstants::SCRIPT_DIR}" +
    "/../lib/dict/words"
  not_a_word =
    ``.empty?

  has_nonalpha = password =~ /[^[:alpha:]]/
  long = (password.size >= 8)
  has_alpha = password =~ /[[:alpha:]]/

  return(not_a_word and has_nonalpha and long and has_alpha)
end

# Error Messages
NO_OPENVZ_SERVER_ERROR = "There are no OpenVZ server that I know of"
RUN_UPDATE_CACHE_ERROR = "Please run the vz-update-cache script."
