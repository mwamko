function textSelectRange (textObj, iStart, iLength) {

    if (textObj.createTextRange) {
        var oRange = textObj.createTextRange();
        oRange.moveStart("character", iStart);
        oRange.moveEnd("character", iLength - textObj.value.length);
        oRange.select();
    } else if (textObj.setSelectionRange) {
        textObj.setSelectionRange(iStart, iLength);
    }

    textObj.focus();

};
