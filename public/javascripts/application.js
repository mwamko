

function selectAll(id, checked) {
  var el = document.getElementById(id);
  for (var i = 0; i < el.elements.length; i++) {
    if (el.elements[i].type == 'checkbox'){
      el.elements[i].checked = checked;
    }
  }
}

function highlightRow(veid) {
  var tds = document.getElementsByName("veid_" + veid + "_cell");
  var checkbox = document.getElementById("veid_" + veid + "_checkbox");

  for (var i = 0; i < tds.length; i++) {
   
   if (checkbox.checked) {
     tds[i].className = "highlighted_table_cell";
   } 
   else {
     tds[i].className = "table_cell";
   }

  }
}

//Used on the admin page to get list of checked VEIDS from the admin_form
function getSelectedVEs()
{
  var admin_form = document.getElementById("admin_form");
  var selectedVEs = new Array();

  for (var i = 0; i < admin_form.elements.length; i++) {
    if (admin_form.elements[i].checked) {
      selectedVEs.push(admin_form.elements[i].name.match(/\d+/));
    }
  }

  return selectedVEs
}

//confirmAction function:
//Used on the admin page to check actions, especially 'destroys'.
//Also returns error when user wants to act without selecting any checkboxes or
//actions.
function confirmAction(selection)
{
  //sets 'VE' as a default for the parameter if none is provided
  var selection = (selection == null) ? "VE" : selection;
  //the selected_action tag contains the possible actions
  var action=document.getElementById("selected_action");
  //the user's selection is returned as an array index
  var index = action.selectedIndex;
  var admin_form = document.getElementById("admin_form");
  var selectedVEs = getSelectedVEs();

  if (selectedVEs.length <= 0){
    alert("You have not selected a " + selection + ".  Please do so.");
    return false;
  } 

  
  //grab the user's selection by indexing index, and check
  //to see if they are trying to destroy a VE
  actionValue = action.options[index].value
  //The default selection for an action is the word 'Actions...', which means
  //the user didn't choose an action.
  if (actionValue == "Actions...") {
    alert("You have not chosen an action.  Please do so.");
    return false;
  } 
  else if (actionValue =="destroy"){
    choice=confirm("You have chosen to destroy VE(s) " + selectedVEs + "\nMwamko will NOT destroy your VEs unless you press CANCEL.");
    //We want to flip the meaning of an OK or cancel click
    //since the question is worded a bit backwards for safety.
    if (choice) {
      return false;
    } else {
      return true;
    }
  } else {
    return true;
  }
}

function confirmUserAction(action)
{
  //on the user interface, the user can choose multiple actions, but we only
  //want code for destroying right now
  if (action == 'destroy') {
    var destroy_div = document.getElementById("destroy");
    var ve_list = destroy_div.getElementsByTagName("input");
    //go through the inputs found, and find the radio box that is checked.
    for (var i=0; i<ve_list.length; i++) {
      if (ve_list[i].type == 'radio' && ve_list[i].checked) {
        var choice=confirm("You have chosen to destroy VE " + ve_list[i].value  + "\nMwamko will NOT destroy your VEs unless you press CANCEL.");
        //We want to flip the meaning of an OK or cancel click
        //since the question is worded a bit backwards for safety.
        if (choice) {
          //stop_polling is a variable that comes from the user interface that
          //needs to be reset to false if no action proceeds.
          stop_polling=false;
          return false;
        } else {
          return true;
        }
      }
    }
  }
}

// Currently not used

function selectAllInUser(uid) {
  select(uid, true);
}

function unselectAllInUser(uid) {
  select(uid, false);
}

function select(uid, value) {
  var checkboxes = document.getElementsByName("checkboxes_for_uid_" + uid);
  for (i in checkboxes) {
    var first_input_tag = checkboxes[i].getElementsByTagName("input")[0]
    first_input_tag.checked = value;
  }
}

function submit() {
 javascript:document.getElementById('form').submit();
}
