class CreateNetworkInterfaces < ActiveRecord::Migration
  def self.up
    create_table :network_interfaces do |t|
      t.column :veid, :integer, {:null => false}
      t.column :name, :string, {:null => false}
    end
  end

  def self.down
    drop_table :network_interfaces
  end
end
