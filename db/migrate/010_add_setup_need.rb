class AddSetupNeed < ActiveRecord::Migration
  def self.up
    add_column :virtual_environments, 
               :setup_needed, :boolean, 
               {:default => true}
  end

  def self.down
    remove_column :virtual_environments, :setup_needed
  end
end
