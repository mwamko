class LocalIp < ActiveRecord::Migration
  def self.up
    create_table :local_ips do |t| 
      t.column :veid, :integer, {:null => false}
      t.column :ip, :string, {:null => false}
    end

    add_index(:local_ips, :ip, :unique => true)
  end

  def self.down
    drop_table :local_ips
  end
end
