class CreateVirtualEnvironments < ActiveRecord::Migration
  def self.up
    create_table :virtual_environments do |t|      
      t.column :veid,       :integer
      t.column :uid,        :integer
      t.column :status,     :string
      t.column :updated_on, :datetime      
    end
    add_index(:virtual_environments, :veid, :unique => true)
  end

  def self.down
    drop_table :virtual_environments
  end
end
