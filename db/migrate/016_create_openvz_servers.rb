class CreateOpenvzServers < ActiveRecord::Migration
  def self.up
    create_table :openvz_servers do |t|
      t.column :ip,         :string, {:null => false}
      t.column :fp,         :text
      t.column :updated_on, :datetime
    end
  end

  def self.down
    drop_table :openvz_servers
  end
end
