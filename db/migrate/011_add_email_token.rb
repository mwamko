class AddEmailToken < ActiveRecord::Migration
  def self.up
    add_column :users, :email_token, :string
  end

  def self.down
    remove_column :users, :email_token
  end
end
