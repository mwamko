class AddIpColumn < ActiveRecord::Migration
  def self.up
    add_column :virtual_environments, :ip, :string
  end

  def self.down
    remove_column :virtual_environments, :ip
  end
end
