class HomepageController < ApplicationController
  layout 'homepage', :except => ['gadget_login']

  def index
    if session[:mwamko_mode] == ['normal', 'gadget'][1]
      render :action=>'gadget_login', :layout => 'gadget'
    else
      redirect_to :action => 'home'
      flash.keep
    end
  end

  def license
    @text = `cat #{RAILS_ROOT}/LICENSE`
  end

  def submit_donation
    post_data = params.collect{|key,val| "#{key}=#{val}"}.join("&")    
    redirect_to "http://apps.facebook.com/paypalapps/donate.php?#{post_data}"
  end  
  
end
