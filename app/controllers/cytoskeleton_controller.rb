OLD_TIME = Time.mktime(1951)

class CytoskeletonController < ApplicationController
  before_filter :set_mode 
  
  def initialize
    @page_title1 = "Virtual Environments"
    @page_title2 = "Create / Power Up / Power Down / Destroy"
  end

  def index
    redirect_to :action => 'areas'
  end


  def update_areas
    if request.xhr?

      areas
    else
      raise 'This only should be called by AJAX'
    end
  end

  def areas    

    areas = [
	 'templates',
         'stopped',
         'started',
         'destroyed',
         'destroy',

         'creating',
         'starting',
         'stopping',
         'destroying'
        ]


    @notices ||= []

    @areas = {}
    @updated_on = {}

    areas.each do |a|
      @areas[a] = []
      @updated_on[a] = OLD_TIME
    end    

    @areas['templates'] = GET_TEMPLATES.call[0]
    @updated_on['templates'] = OLD_TIME

    all = VirtualEnvironment.
      find_all_by_uid(session[:user_id],
                      :order => 'updated_on DESC').each do |i|

      s = AreaItem.new
      s.caption = i.veid.to_s
      s.veid = i.veid 
      s.ip = i.ip
      
      @areas[i.status] ||= []
      @areas[i.status] << s

      @updated_on[i.status] ||= OLD_TIME
      if i.updated_on > @updated_on[i.status]
        @updated_on[i.status] = i.updated_on 
      end
    end

    @areas['destroy'] = @areas['stopped']
    @updated_on['destroy'] = @updated_on['destroyed']


    ves = VirtualEnvironment.
      find_all_by_uid_and_status(session[:user_id], 'started') || []

    @tags = {}
    ves.each do |ve|
      @tags[ve.veid] = []
      OpenVZ.ls(ve.veid, "~#{session[:uname]}").each do |path|
        @tags[ve.veid] << path
      end
    end

  end

  
  def ve_info
    @menu = find_menu

    internal = Internal.new(params[:id])

    @disk_spaces = internal.disk_spaces
    @uptime = internal.uptime
    @memory = internal.memory

  end



  
  def close_details
    raise 'No item specified' unless params[:id]
    
    @menu = find_menu
    
    #close the box for the given product
    @menu.item_state[params[:id].to_sym] = 'closed'
    
    redirect_to :action => :ve_info, :id => params[:veid]
  end
   
  def open_details
    raise 'No item specified' unless params[:id]     
    @menu = find_menu
    
    #open the box for the given product
    @menu.item_state[params[:id].to_sym] = 'opened'
    
    redirect_to :action => :ve_info, :id => params[:veid]
  end
  



  private

  def set_mode
    #set mwamko to normal mode
    session[:mwamko_mode] = ['normal', 'gadget'][0]
  end
end



class AreaItem
  attr_accessor :template, :veid, :caption, :name, :path, :ip, :update_on
end

