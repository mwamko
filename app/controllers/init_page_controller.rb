require File.expand_path("#{RAILS_ROOT}/vendor/open4/lib/open4")

class InitPageController < ApplicationController
  before_filter :only_for_admin
  before_filter :set_page_title
  layout "admin"

  def index
    #Retrieving or generating Key for admin
    private_key_path = "#{RAILS_ROOT}/tmp/mwamko_id_rsa"
    public_key_path = "#{private_key_path}.pub"
    
    existence = [File.exists?(File.expand_path(public_key_path)),
                 File.exists?(File.expand_path(private_key_path))]
    if existence == [false, false]
      `#{SSH_KEYGEN} -t rsa -f #{private_key_path} -N ''`
    elsif existence == [true, true]
    else
      raise "Something is wrong"
    end

    @server_key=`cat #{public_key_path}`.strip
    raise "Something is wrong with ssh-keygen" if @server_key.empty?

    @host=OpenvzServer.find_all.last
  end

  def validate_key

    @ip = params['ip'].strip
    if !(@ip =~ SAFE_IP4)
      flash[:notices] ||= []
      flash[:notices] <<  "#{@ip} is not a valid ip"
      redirect_to :controller => 'init_page', :action => 'index'
      return false
    end

    # Using popen4 in block form so that the child process is waited for
    # otherwise it might not be properly handled after it dies and end-up
    # as a zombie process.
    # Please don't put return statements inside the block.
    error = ''
    out = ''     
    cmd = "#{SSH_KEYSCAN} -t rsa #{@ip}"
    status = Open4::popen4(cmd) do |pid,i,o,e|
      error = e.read # this may take up to 5 seconds
      out = o.read
      i.close
      o.close
      e.close
    end

    if !error.nil? and !(error =~ /\A#.*\Z/)
      error = if error.empty?
                'No results.'
              else
                error
              end
      flash[:notices] ||= []
      flash[:notices] <<  "ssh-keyscan: #{error}\nTry a different host name."
      redirect_to :controller => 'init_page', :action => 'index'
      return false
    end
    
    open("#{RAILS_ROOT}/tmp/new_server_key.pub","w") do |key|
      key.write(out)
    end    
    @finger_p = `#{SSH_KEYGEN} -lf #{RAILS_ROOT}/tmp/new_server_key.pub`

  end

  def add_openvz_server
    if params['response']=="yes"
      OpenvzServer.create(:ip => params['ip'].strip, :fp =>params['fp'])
      open("#{RAILS_ROOT}/tmp/ssh_known_openvz_hosts","a") do |hosts| 
        hosts.write(params['new_key'])
      end
      flash[:notices] ||= []
      flash[:notices] << "New OpenVZ server added."      
    else
      flash[:notices] ||= []
      flash[:notices] << "Action canceled."
    end
    redirect_to :controller => 'init_page', :action => 'index'
  end

  private 
  
  def set_page_title
    @page_title1 = "Mwamko Administration"
    @page_title2 = "Connectiong to the OpenVZ Server"
  end

end
