class AdminController < ApplicationController
  before_filter :only_for_admin
  before_filter :set_page_title
  before_filter :set_mode 

  
  def index
    redirect_to :action => "global_table"
  end

  
  def global_table
    @order_list = (@params['order_list']||'').split('/').collect {|i| i.to_sym}
    @order_directions = (@params['order_directions']||'').split('/')

    # Default Order
    if @order_list.empty?
      @order_list = [:is_admin, :veid]
      @order_directions = ['ASC', 'DESC']
    end

    @notices ||= []
    
    @user_columns = [:name, :institution, :is_admin, :labels]
    @ve_columns = [:status, :veid, :ip, :label]

    shown_statuses = 
      ['started', 'stopped', 'stopping', 'starting', 'destroying']

    virtual_environments = VirtualEnvironment.
      find_all_by_status(shown_statuses)

    users = User.find(:all)

    # Building the @rows array
    @rows = []
    virtual_environments.each do |ve|
      user = User.find_user_by_id(users, ve.uid)
      
      @rows << {}
      @user_columns.each do |c| 
        @rows.last[c] = case c
                        when :institution
                          # user[:email] =~ SAFE_EMAIL
                          # $3
                        when :labels
                        else
                          user.method(c).call
                        end
      end

      @ve_columns.each do |c| 
        @rows.last[c] = case c
                        when :label
                        else
                          ve.method(c).call
                        end
      end

    end


    # Sorting the @rows array

    def comparison(first, second) 
      human2digit = {'ASC' => 1, 'DESC' => -1}

      @order_list.each_with_index do |o, i|
        human_direction = @order_directions[i]

        formated_first, formated_second = * case o
                                            when :is_admin
                                              m = {true=>1, false=>0}
                                              [ # booleans
                                               m[first[o]], 
                                               m[second[o]]
                                              ]
                                            when :veid
                                              [ # integers
                                               first[o], 
                                               second[o]
                                              ]
                                            else
                                              [ 
                                               first[o].to_s, 
                                               second[o].to_s
                                              ]
                                            end

        # This will be 1, -1, or 0 
        cmp = (formated_first <=> formated_second)

        if cmp != 0
          return cmp * human2digit[human_direction]
        end
      end

      return 0 # Elements are equal
    end

    @rows.sort!{ |f,s| comparison(f,s) }


  end

  def action   

    flash[:notices] ||= []
    has_selection = false
    
    params.each_pair do |key, val|
      if key =~ /checkbox_for_([a-z]+)_([0-9a-z_\-.]+)/ and val == "on"
        has_selection = true
        case $1 
        when 'veid'
          ve = VirtualEnvironment.find_by_veid($2.to_i)
        when 'name'
          name =  User.find_by_name($2)
        end 
          
        
        session[:workers] ||= {}
        if 'destroy'== params[:selected_action]
          if ve.status == 'stopped'
            session[:workers][ve.veid] = 
              Nucleus.new.destroy(ve.veid), 'destroying'
          else
            flash[:notices] << "Error Destroy: VE #{ve.veid} is not stopped"
          end
        elsif 'stop' == params[:selected_action]
          if ve.status == 'started'
            session[:workers][ve.veid] = Nucleus.new.stop(ve.veid), 'stopping'
          else
            flash[:notices] << "Error Stop: VE #{ve.veid} is not started"
          end        
        elsif 'start' == params[:selected_action]
          if ve.status == 'stopped'
            session[:workers][ve.veid] = Nucleus.new.start(ve.veid), 'starting'
          else
            flash[:notices] << "Error Start: VE #{ve.veid} is not stopped"
          end
        elsif 'disable' == params[:selected_action]
          if name.is_disabled
            flash[:notices] << 
              "Error Disable: #{name.name} is already disabled."
          else 
	    name.is_disabled = true
            
            name.save!
          end
        elsif 'enable' == params[:selected_action]
          if !name.is_disabled
            flash[:notices] << "Error Enable: #{name.name} is already enabled."
          else
            name.is_disabled = false
            name.save!
            #code here to stop all virtual machines by the user
          end
        else
          render (:inline => "<%=simple_message('Action was not selected.')%>",
                  :layout => 'application')          
          return false
        end 
      end    
    end


    if !has_selection
      render (:inline => "<%=simple_message('No VEs were selected.')%>",
              :layout => 'application')
      return false
    else 
      redirect_to :controller => 'admin', :action => params[:come_back_to]
      return false  
    end
  end


  def account_management
    @names = User.find_all.collect{|u| u.name}.sort
  end


  def hn_statistics
    # @body = `cat /home/alevchuk/sstats/aggregatedsstats.txt`
  end


  private 
  
  def set_page_title
    @page_title1 = "Mwamko Administration"
    @page_title2 = params[:action].gsub('_', ' ').titleize
  end

  def set_mode
    #set mwamko to normal mode
    session[:mwamko_mode] = ['normal', 'gadget'][0]
  end

end
