class InternalController < ApplicationController

  def initialize
    @page_title1 = "Mwamko: " << self.class.to_s
    @page_title2 = "
          We&rsquo;re running 
          <i>Revision #{Development.revision}</i>
          in
          <i>#{Development.owner}</i>"
  end

  def index
    list
    render :action => 'list'
  end

  def list
    @list = List.new
  end
end

def process_error(msg)
  logger.error(msg) 
  flash[:notice] = msg
  redirect_to :action => :index 
end
