class DevelopmentController < ApplicationController

  def initialize
    @page_title1 = "Mwamko Development Page"
  end

  def index
    @rails_log = Development.rails_log
    @git_log = Development.git_log
  end

end
