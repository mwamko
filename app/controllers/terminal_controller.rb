require 'open3'

class TerminalController < ApplicationController
  layout "application", :only => :traditional_terminal
  before_filter :set_page_title

  private
  def regular_expression_check(variable_name, re)
    raise "Wrong #{variable_name}=" +
      "#{instance_variable_get(variable_name).inspect}" unless
      instance_variable_get(variable_name).to_s =~ re
  end


  public


  def index
    redirect_to :controller => 'cytoskeleton', :action => 'areas'
  end


  def traditional_terminal

    # Initializations

    @user_id = session[:user_id].to_i
    @ve_id = params[:id]

    # ====================================================== @ve_id Checks ===
    # Basic check for the incomming VE value
    regular_expression_check("@ve_id", SAFE_VEID)
    ve = VirtualEnvironment.find_by_veid_and_uid(@ve_id, session[:user_id])
    raise "VE #{@ve_id.inspect} dose not exist." if ve.nil?

    # Check for the presence of the IP
    @ve_ip = ve.ip
    raise "VE #{@ve_id.inspect} dose not have an IP." if
      @ve_ip.nil? or @ve_ip.strip == ''

    # ========================================================================
    # ========================================================================

    # ==================================================== @user_id Checks ===
    # Basic check for the incomming User ID value
    regular_expression_check("@user_id", SAFE_UID)
    user = User.find_by_id(@user_id)
    raise "User #{@user_id.inspect} does not exist." if user.nil?

    # Check for the presence of the name
    @user_name = user.name
    raise "User #{@user_id.inspect} dose not have a name." if
      @ve_ip.nil? or @ve_ip.strip == ''

    # ================================================ relationship Checks ===
    # Check for correct VE to User accissiation
    if ve.uid != @user_id
      raise "VE #{@ve_id.inspect} does not belong to User #{@user_id.inspect}."
    end

    # ========================================================================
    # ========================================================================

    @rsa_fingerprint = OpenVZ.rsa_fingerprint(ve.veid)
    @dsa_fingerprint = OpenVZ.dsa_fingerprint(ve.veid)


  end

  def ajaxterm
    @ve_id = params[:id]

    # ====================================================== @ve_id Checks ===
    # Basic check for the incomming VE value
    regular_expression_check("@ve_id", SAFE_VEID)
    ve = VirtualEnvironment.find_by_veid_and_uid(@ve_id, session[:user_id])
    raise "VE #{@ve_id.inspect} dose not exist." if ve.nil?

    # Check for the presence of the IP
    @ve_ip = ve.ip
    raise "VE #{@ve_id.inspect} dose not have an IP." if
      @ve_ip.nil? or @ve_ip.strip == ''

    # ========================================================================
    # ========================================================================

    @rsa_fingerprint = OpenVZ.rsa_fingerprint(ve.veid)
    @dsa_fingerprint = OpenVZ.dsa_fingerprint(ve.veid)

    @ajaxterm_path = "/terminal/#{@ve_ip}" # Absolute Path the the Rails of
                                          # of the UNIX user mwamko
    render :layout=>'ajaxterm'
  end


private


  def set_page_title
    @page_title1 = "User-VE Interaction"
    @page_title2 = params[:action].gsub('_', ' ').titleize
  end

end
