require 'password_gen'

class LoginController < ApplicationController
  before_filter :authorize, :except => [:login,
                                        :logout,
                                        :create_student_account,
                                        :new_account_confirmation,
                                        :forgot_passwd,
                                        :find_user,
                                        :token,
                                        :invalid_token,
                                        :unknown_user,
                                        :invalid_email]

  # For these functions should not be accessiblase
  #
  before_filter :disable, :only => [:add_user, :delete_user]

  layout 'application', :except => 'login'

  def create_student_account

    def is_allowed?(user_email)

      user_email =~ /^.+@(.+)$/
      user_host = $1

      File.open(ALLOWED_EMAILS_FILE).each do |raw_line|
        line = raw_line.strip
        return true if user_email == line

        # Support for * matching
        if line =~ /^\*@(.+)/
          return true if user_host == $1
        end

      end

      return false
    end

    raise "Invalid Request. Expecting a specific POST" unless
      params.include?(:user) and request.post?

    return show_error("Please Enter an Email") if params[:user][:email] == ''

    # SECURITY: Input Check
    unless params[:user][:email] =~ SAFE_EMAIL
      return show_error("Please Enter a Valid Email")
    else
      # De-coupled from the details of the SAFE_EMAIL regular expression
      if params[:user][:email] =~ /^([^@]+)@(.+)$/
        username = $1
        hostname = $2
        email = "#{$1}@#{$2}"
      else
        raise "Could not split the email into the 2 parts."
      end
    end


    return show_error("Your Email is Not Authorized") unless is_allowed?(email)

    new_password = PasswordGen.generate_pronounceable


    new_user = User.new(:name => email,
                        :password => new_password,
                        :password_confirmation => new_password)

    unless new_user.save
      flash[:notices] ||= []
      new_user.errors.full_messages.each do |msg|
        flash[:notices] << msg
      end

      return redirect_back
    end

    # Send Out Email
    NewStudent.deliver_login_info(email, username, new_password)

    redirect_to :action => 'new_account_confirmation'

  end

  def add_user
    @user = User.new(params[:user])
    if request.post? and @user.save
      flash[:notices] ||= []
      flash[:notices] << "User #{@user.name} created"
      @user = User.new
    end
  end

  def delete_user
    id = params[:id]
    if id && user = User.find(id)
      flash[:notices] ||= []
      begin
        user.safe_delete
        flash[:notices] << "User #{user.name} deleted"
      rescue Exception => e
        flash[:notices] << e.message
     end
    end
    redirect_to(:action => :list_users)
  end

  def list_users
    @all_users = User.find(:all)
  end

  def login
    session[:user_id] = nil
    if !request.post?
      raise "Expecting a POST request"
    end



    user = User.authenticate(params[:name], params[:password])


    if user
      session[:user_id] = user.id

      if session[:mwamko_mode] == ['normal', 'gadget'][1]
        redirect_to(:controller => 'gadget')
      elsif User.find_by_id(session[:user_id]).is_admin
        redirect_to(:controller => 'admin')
      else
        redirect_to(:controller => "cytoskeleton")
      end

    else

      redirect_to(:controller => 'homepage')
      flash[:notices] ||= []
      flash[:notices] << "Invalid email/password combination."

      return 0
    end

  end

  def logout
    unless session[:workers].nil? or session[:workers].empty?
      add_error("Please wait. Some things are still in progress...")
      if session[:mwamko_mode] == ['normal', 'gadget'][1]:
        redirect_to :controller => 'gadget', :action => 'areas'
      else
        redirect_to :controller => 'cytoskeleton', :action => 'areas'
      end
      return false
    end

    unless session[:user_id].nil?
      #remember the session mode before resetting, then set it back after.
      temp_sessionmode = session[:mwamko_mode]
      reset_session
      session[:mwamko_mode] = temp_sessionmode
      redirect_to
      return false
    end

    add_error "Logged out"
    if session[:mwamko_mode] == ['normal', 'gadget'][1]:
      redirect_to :controller => 'gadget'
    else
      return redirect_home
    end
  end

  def find_user
    user = params['user']
    email = user['email']
    #get user name from the email address
    regexp = /@/
    ar = []
    error = "@ was not in the email address"
    email = email.to_s
    if(email =~ regexp)
      ar = email.split("@")
      user_name = ar[0]
      @found = User.exists?(:name => "#{user_name}")
      if(@found == true)
        # generate a random string containing letters and number
        email_token = PasswordGen.generate_pronounceable(16)
        # insert token into database
        record = User.find_by_name("#{user_name}")

        record.email_token = email_token
        record.save!

        #send email
        NewStudent.deliver_reset_password(email, user_name, email_token)
      else
        redirect_to(:controller => 'login',
                    :action => 'unknown_user',
                    :email => email)
      end
    else
      redirect_to(:controller => 'login',
                  :action => 'invalid_email',
                  :email => email)
    end
  end

  def token
    token = params[:id]
    #make sure token is valid
    record = User.find_by_email_token(token)
    if(record.nil?)
      #appropiate error message and redirection
      redirect_to(:action => 'invalid_token')
    else
      #generate new password and email it
      email = record.name + "@cs.ucr.edu"
      new_password = PasswordGen.generate_pronounceable
      record.password_confirmation = new_password
      record.password = new_password
      record.email_token = nil
      record.save!
      NewStudent.deliver_new_password(email, record.name,new_password)
    end
  end

  def change_password
    password_1 = params[:password_1]
    password_2 = params[:password_2]


    # simple checks to see if the passwords the user provided match and
    # that they are not blank
    if(password_1.empty? and password_2.empty?)
      redirect_to({:action => 'blank_fields'})
    elsif(password_1 == password_2)

      def password_1.is_safe?
        # Alphanumeric or !@#$%^&*()_+-= or comma or dot or space
        self =~ SAFE_PASSWORD
      end

      if !password_1.is_safe?
        redirect_to :action => 'unsafe_password'
      else

        def password_1.is_strong?
          PASSWORD_IS_STRONG(self)
        end

        if !password_1.is_strong?
          redirect_to :action => 'weak_password'
        else
          record =  User.find(session[:user_id])
          record.password_confirmation = password_1
          record.password = password_1
          record.save!
          redirect_to({:action => 'success'})
        end

      end

    elsif(password_1 != password_2)
      redirect_to({:action => 'no_match'})
    end
  end


  private

  def redirect_home(keywords={})

    redirect_to({:controller => 'homepage',
                  :action => 'about'}.merge(keywords))
    return false
  end

  def redirect_back(keywords={})
    redirect_to(:back, keywords) rescue redirect_home
    return false
  end

  def add_error(msg)
    flash[:notices] ||= []
    flash[:notices] << msg
  end

  def show_error(msg)
    add_error(msg)
    return redirect_home()
  end

end
