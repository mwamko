# Filters added to this controller will be run for all 
# controllers in the application.
# Likewise, all the methods added will be available for all controllers.

PUBLICLY_ACCESSIBLE_PAGES = [
                             'homepage',
                             'login/login',
                             'login/logout',
                             'gadget/google_xml'
                            ]


class ApplicationController < ActionController::Base


  before_filter :authorize # This line must never be removed. 
                           # For more info read the definition of: authorize
  before_filter :check_setup
  before_filter :initialize_uname

  private

  def check_setup
    # TODO: Remove duplication - there is a same thing in authorize

    # If it is one of the publicly pages then stop here.
    PUBLICLY_ACCESSIBLE_PAGES.each do |page|
      page_elements = page.split('/')
      if page_elements.size == 1
        return true if params[:controller] == page_elements[0]
      elsif page_elements.size == 2
        return true if [params[:controller], params[:action]] == page_elements
      else
        raise "Strange publicly_accessible_pages:\n" +
          publicly_accessible_pages.inspect
      end
    end

    if !session[:user_id].nil? # User is logged in
      if(params[:controller]!="init_page" and OpenvzServer.find(:all).empty?)
        flash[:notices] ||= []
        if User.find(session[:user_id]).is_admin
          flash[:notices] << "No OpenVZ servers are configured" 
          redirect_to(:controller => "init_page") 
        else
          flash[:notices] << 
            "Mwamko is not\nconfigured.\n\n" +
            "Please login as admin."
          redirect_to(:controller => "homepage")           
        end
        return false
      end
    end
  end

  # Plase edit funtion carefuly - this is the core of the
  # authentication of our website.
  def authorize 
    
    # If it is one of the publicly pages then stop here.
    PUBLICLY_ACCESSIBLE_PAGES.each do |page|
      page_elements = page.split('/')
      if page_elements.size == 1
        return true if params[:controller] == page_elements[0]
      elsif page_elements.size == 2
        return true if [params[:controller], params[:action]] == page_elements
      else
        raise "Strange publicly_accessible_pages:\n" +
          publicly_accessible_pages.inspect
      end
    end

      
    # If the user is already Logged In then stop here.
    if User.find_by_id(session[:user_id]) 
      return 
    end


    # If the user was accessing the gadget and was not Logged In
    # then session[:mwamko_mode] should be set accordingly before
    # redicting the Login Page.
    if params[:controller] == 'gadget'
      session[:mwamko_mode] = ['normal', 'gadget'][1]
    end


    # Redirect to the Login page
    # This is a very important line in the security of our website.
    return disable

  end


  # This must not be changed.
  # For more info read the definition of: authorize
  def disable 
    flash[:notices] ||= []
    flash[:notices] << "Login Required" 
    redirect_to(:controller => "homepage") 
    return false
  end


  def only_for_admin
    raise "you must be logged in" if session[:user_id].nil?
    if !User.find_by_id(session[:user_id]).is_admin
      raise "This is only for Admins"
    end
  end
  
  def initialize_uname
    uname = User.find_by_id(session[:user_id])
    if uname
      session[:uname] = uname.name
    end
  end

  def find_menu
    session[:menu] ||= Menu.new
  end
end
