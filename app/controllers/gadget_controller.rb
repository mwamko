class GadgetController < CytoskeletonController
  layout "gadget", :except => "google_xml"
  before_filter :set_mode 
  
  def index
    redirect_to :action => 'areas'
  end

  def areas
    super
  end

  def google_xml
    render :template => "gadget/google_xml"
  end

  def set_mode
    #set mwamko to gadget mode
    session[:mwamko_mode] = ['normal', 'gadget'][1]
  end
end
