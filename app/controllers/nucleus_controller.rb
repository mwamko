class NucleusController < ApplicationController
 
  def index

    if params[:button].nil?
      raise 'Nucleus is expecting params[:button]. But no value was passed.' 
    end

    self.method(params[:button].downcase.gsub(' ', '_')).call
   
  end

  def poll
    # Exposes the private method of ApplicationController
    super
  end

  
  def create
    if params['template'].nil?
      process_error("You haven&rsquo;t selected a template to create")
    else

      user = User.find_by_id(session[:user_id])
      raise "No such user" if user == nil
      uid = user.id
        
      errors = Nucleus.new.create(uid)
      if !errors.empty?
        flash[:notices] ||= []
        flash[:notices] << errors
      end
         
    end

    #If we are using the google gadget, redirect there,
    #not to cytoskeleton
    if session[:mwamko_mode] == ['normal', 'gadget'][1]
      redirect_to :controller => 'gadget', :action => 'areas'
    else
      redirect_to :controller => 'cytoskeleton', :action => 'areas'
    end
  end

  def start
    veid = @params[:id]
    
    if veid.nil?
      process_error("You haven&rsquo;t selected a computer to start")
    else
      errors = Nucleus.new.start(veid)
      if !errors.empty?
        flash[:notices] ||= []
        flash[:notices] << errors
      end  
    end

    if session[:mwamko_mode] == ['normal', 'gadget'][1]
      redirect_to :controller => 'gadget', :action => 'areas'
    else
      redirect_to :controller => 'cytoskeleton', :action => 'areas'
    end
  end
  
  def stop      
    veid = @params[:id]
    if veid.nil?
      process_error("You haven&rsquo;t selected a VM to stop")
    else
      errors = Nucleus.new.stop(veid)
      if !errors.empty?
        flash[:notices] ||= []
        flash[:notices] << errors
      end  
    end

    if session[:mwamko_mode] == ['normal', 'gadget'][1]
      redirect_to :controller => 'gadget', :action => 'areas'
    else
      redirect_to :controller => 'cytoskeleton', :action => 'areas'
    end
  end  
  
  def destroy
    veid = @params[:id]
    if veid.nil?
      process_error("You haven&rsquo;t selected a VM to destroy")
    else
      errors = Nucleus.new.destroy(veid)
      if !errors.empty?
        flash[:notices] ||= []
        flash[:notices] << errors
      end
    end

    if session[:mwamko_mode] == ['normal', 'gadget'][1]
      redirect_to :controller => 'gadget', :action => 'areas'
    else
      redirect_to :controller => 'cytoskeleton', :action => 'areas'
    end
  end



  def reset_password    
    
    flash[:notices] ||= []
    
    if request.post?
      raise "Wrong #{params[:id].inspect}" unless params[:id].to_s =~ SAFE_VEID
      
      unless params[:id] =~ SAFE_USERNAME
        raise "Invalid username #{params[:id].inspect}"
      end
      
      if(params[:password].empty?)
        redirect_to({:controller=>'login', :action => 'blank_fields'})
        return false
      end

      unless params[:password] =~ SAFE_PASSWORD
        redirect_to :controller=>'login', :action => 'unsafe_password'
        return false
      else        
        if !PASSWORD_IS_STRONG(params[:password])
          redirect_to :controller=>'login', :action => 'weak_password'
          return false
        else
          OpenVZ.set_ve_userpassw(params[:id], 
                                  params[:username],
                                  params[:password])
          
          flash[:notices] << "Password Changed"

          if session[:mwamko_mode] == ['normal', 'gadget'][1]
            redirect_to :controller => 'gadget', :action => 'areas'
          else
            redirect_to :controller => 'cytoskeleton', :action => 'areas'
          end

          return false
        end
      end

      raise "Post was not processed"
    end

    @users = OpenVZ.list_users(params[:id])

  end
  
  def upgrade
    veid = params[:id]
    session[:workers] ||= {}
    session[:workers][veid] = Nucleus.new.upgrade(veid), 'upgrading'

    redirect_to :controller => 'cytoskeleton', :action => 'areas'
  end


  def add_ip 
    veid = params[:id]
    ip = Nucleus.new.add_ip(veid)

    flash[:notices] ||= []
    flash[:notices] << "#{ip} was added to VE #{veid}"

    redirect_to :controller => 'cytoskeleton', :action => 'areas'
  end

  def add_network_interface
    flash[:notices] ||= []
    veid = params[:id]
    name = params[:name]

    begin
      Nucleus.new.add_netif(veid, name)
    rescue => msg
      flash[:notices] << msg
      redirect_to :controller => 'cytoskeleton', :action => 'areas'
      return
    end

    flash[:notices] << "#{name} was added to VE #{veid}"

    redirect_to :controller => 'cytoskeleton', :action => 'areas'
  end

end



# Supplementary

def redirect_to_list(msg)
  flash[:notices] ||= []
  flash[:notices] << msg
end

def process_error(msg)
  logger.error(msg) 
  redirect_to_list(msg)
end
