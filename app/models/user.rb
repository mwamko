# Mwamko  Copyright (C) 2007  Aleksandr O. Levchuk

require 'digest/sha1'
MIN_PASSWORD_SIZE  = 8


class User < ActiveRecord::Base
  attr_accessor            :password_confirmation
  validates_uniqueness_of  :name
  
  ## Let's not deal with this for now
  # has_many :virtual_environments

  # Safety Checks
  # The tests for SAFETY should be done before validation
  # because functions like "validates_uniqueness_of" send
  # the strings to outside (out to the DB) while they are
  # invalid.

  def before_validation
    if name !~ SAFE_EMAIL
      write_attribute(:name, 'email is not safe') # Rewrite the :name variable
    end
  end

  # Take over the failed SAFETY Checks
  validates_exclusion_of :name,
                         :in => ['email is not safe'],
                         :message => "should be a valid email"



  def self.authenticate(name, password)
    user = self.find_by_name(name)
    if user
      expected_password = encrypted_password(password, user.salt)
      if user.hashed_password != expected_password
        user = nil
      end
    end
    user
  end

  def destroy

    VirtualEnvironment.find_all_by_uid(self.id).each do |ve|
      Nucleus.new.stop(ve.veid)
    end

    VirtualEnvironment.find_all_by_uid(self.id).each do |ve|
      Nucleus.new.destroy(ve.veid)
      ve.destroy
    end

    super
  end

  def safe_delete
    transaction do
      destroy
      if User.count.zero?
        raise "Can't delete last user"
      end
    end
  end

  def password
    @password
  end

  def password=(pwd)
    @password = pwd
    create_new_salt
    self.hashed_password = User.encrypted_password(self.password, self.salt)
  end


  # Pulls out a user from a list of User models
  # without accessing the database
  def self.find_user_by_id(users, id)
    user_IDs = users.inject([]) do |bucket, i|
      if i.id == id
        bucket << i
      else
        bucket
      end
    end

    if user_IDs.size > 1
      raise "Found more then one user with id=#{id.inspect}"
    end

    if user_IDs.size == 0
      raise ["There is/are VE(s) belonging to user with id=#{id.inspect}",
             "but such user does not exists.",
             "",
             "Here is one such VE:",
             VirtualEnvironment.find_by_uid(id).inspect
            ].join("\n")
    end

    return user_IDs[0]
  end


  private

  def validate
    if password.nil? == false # if password was supplied just now
      errors.add(:password,"should not be 0") if
        password.size == 0

      errors.add(:password,
                 "should be at least #{MIN_PASSWORD_SIZE} characters long") if
        password.size < MIN_PASSWORD_SIZE

      errors.add(:password_confirmation,"should be same as password") if
        password != password_confirmation
    end
  end

  def self.encrypted_password(password, salt)
    string_to_hash = password + "Edvard Munch" + salt
    Digest::SHA1.hexdigest(string_to_hash)
  end

  def create_new_salt
    self.salt = self.object_id.to_s + rand.to_s
  end

end
