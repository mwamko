require 'kilobytes2human'

def make_human_methods (method_id)
  rexp = /_human$/
  
  if method_id.to_s =~ rexp
    method_id = method_id.to_s.gsub(rexp, '')
    out = Kilobytes2Human.convert(method(method_id).call)
  else
    out = method(method_id).call
  end
  
  return out
end

class Internal < ActiveRecord::Base
  attr_reader :disk_spaces, :uptime, :memory

  def initialize (id) 
    @disk_spaces = DiskSpaces.new(id)
    @uptime = UpTime.new(id)
    @memory = Memory.new(id)
  end

end

class DiskSpaces

  def initialize (id)
    @raw_data = OpenVZ.df(id)
    if @raw_data == ""
      raise MachineDoesNotExistError, 
      "Invalid VEID: VE #{id} doesn't exist on the system."
    end
  end
  
  def each
  
    lines = @raw_data.split("\n")[1..-1]
      
    lines.each do |line|
      line_of_numbers = line.scan(/[0-9]+/)
      line_of_chars = line.split(" ")


      yield DiskSpace.new(:total => line_of_numbers[0].to_i,
                          :available => line_of_numbers[2].to_i, 
                          :fs_type => line_of_chars[0], 
                          :used => line_of_numbers[1].to_i, 

                          :use_percentage => "#{line_of_numbers[3]}%", 
                          
                          :mount_on => line_of_chars[5])

    end 
  end

  def each_with_index 
    index = 0 
    each do |i|
      yield i, index
      index =+ 1
    end
  end
end

class UpTime 

  attr_reader :uptime, :users, :cpu_load_1,
              :cpu_load_5, :cpu_load_15
  
  def initialize(id)
    puts "--->Initializing an object of class UpTime"

    @raw_data = OpenVZ.uptime(id)
  
    # Example: 
    # 20:52:47 up 18 days,  6:13,  4 users,  load average: 0.00, 0.26, 0.46

    if @raw_data =~ /.*? up (.*? users),/
      $1 =~ /(.*),(.*?) users/
      @uptime = $1
      @users = $2
    else
      @uptime = @users = "Not found"
    end
    
    loads = @raw_data.scan(/[0-9]+\.[0-9]+/)
    @cpu_load_1 = loads[0].to_f
    @cpu_load_5 = loads[1].to_f
    @cpu_load_15 = loads[2].to_f
  end
end

class Memory

  attr_reader :total, :available, :buffers, :cached

  def initialize(id)
    @raw_data = `#{SUDO} #{VZTOOLS_PATH}/vzctl exec #{id} free -o`
   
    lines = @raw_data.split("\n")[1...-1]
    lines.each do |line|
      data = line.scan(/[0-9]+/)      
      @total = data[0].to_i
      @available = data[2].to_i
      @buffers = data[4].to_i
      @cached = data[5].to_i
    end    
  end
  
  def method_missing(method_id)
    make_human_methods(method_id)
  end
end

class DiskSpace

  attr_reader :fs_type, :mount_on, :total, :used, :available, :use_percentage

  def initialize(keyword_arguments)
    keyword_arguments.each_pair do |key, value|
      if !public_methods.include?(key.to_s)
        raise "#{key.inspect} is not a public method of #{self.class}"
      end
      
      instance_variable_set("@#{key}", value)
    end
  end
  
  def method_missing(method_id)
    make_human_methods(method_id)
  end
end
