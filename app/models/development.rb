class Development

  DEVELOPMENT_LOG = "#{RAILS_ROOT}/log/development.log"

  def self.rails_log
    `#{TAIL} -n 1000 #{DEVELOPMENT_LOG}`
  end

  def self.git_log
    cmd = "cd #{RAILS_ROOT }; #{GIT} log --pretty=medium --color"
    `#{cmd}`
  end

  def self.git_log_short
    cmd = "cd #{RAILS_ROOT }; #{GIT} log -1 --pretty=short --color"
    `#{cmd}`
  end
end
