require 'password_gen'

class Nucleus

#Note on create, start, stop, and destroy functions: They are built on scripts
#that Alex programmed, which return output only on error (this is why there are
#"2>&1" directives in all of the functions). The programs return nothing if the
#initial stages are successful, but after they daemonize, any potential errors
#are stored in files in the /tmp file under a devised naming scheme.
#
#The functions operate by calling Alex's scripts through bash, hence the
#backticks and sudo. A null string is returned on initial success, and a string
# with an error message on failure.


  def create(uid)

    raise "Wrong uid=#{uid.inspect}" unless
      (uid.to_s =~ SAFE_UID)

    ve = VirtualEnvironment.new
    VirtualEnvironment.transaction do
      # Transactions exhibit the ACID properties
      # And need to be used when there are multiple SQL statements
      # that are doing all doing one thing

      ve.uid = uid.to_i

      begin
        # 1st SQL statement
        ve.grab_available_veid

      rescue
        return 'No more VEs can be created.'

      else
        ve.status = 'creating'
        ve.updated_on = Time.now
        ve.ip = nil

        # 2nd SQL statement
        ve.save!
      end
    end

    errors = `#{RAILS_ROOT}/script/mwamko-fast-create #{ve.veid} 2>&1`

    if !errors.strip.empty?
      ve.destroy
    end

    return errors
  end

  def start(veid)
    ve = VirtualEnvironment.find_by_veid(veid)
    old_status = ve.status
    ve.status = 'starting'
    ve.save!

    errors = `#{RAILS_ROOT}/script/mwamko-start #{veid} 2>&1`

    if !errors.strip.empty?
      if errors =~ /already running/
        ve.status = 'started'
      else
        ve.status = old_status
      end
      ve.updated_on = Time.now
      ve.save!
    end

    return errors
  end

  def stop(veid)
    ve = VirtualEnvironment.find_by_veid(veid)
    old_status = ve.status
    ve.status = 'stopping'
    ve.save!

    errors = `#{RAILS_ROOT}/script/mwamko-stop #{veid} 2>&1`

    if !errors.strip.empty?
      if errors =~ /already stopped/
        ve.status = 'stopped'
      else
        ve.status = old_status
      end
      ve.updated_on = Time.now
      ve.save!
    end

    return errors
  end

  def destroy(veid)
    ve = VirtualEnvironment.find_by_veid(veid)
    old_status = ve.status
    ve.status = 'destroying'
    ve.save!

    errors = `#{RAILS_ROOT}/script/mwamko-destroy #{veid} 2>&1`

    if !errors.strip.empty?
      ve.status = old_status
      ve.updated_on = Time.now
      ve.save!
    end

    return errors

  end

  def reset_pass(veid, uname)
    upass = PasswordGen.generate_pronounceable
    OpenVZ.set_ve_userpassw(veid, uname, upass)

    return upass
  end


  def add_ip(veid)
    ve = VirtualEnvironment.find_by_veid(veid)

    ip = ve.grab_available_local_ip
    OpenVZ.add_local_ip(veid, ip)

    ve.save!
    return ip
  end


  def add_netif(veid, name)
    OpenVZ.add_netif(veid, name)
    if NetworkInterface.find_by_veid_and_name(veid, name)
      raise ["#{name} already exists in VE #{veid}.",
             "Perhaps a different name will work."].join("\n")
    end
    NetworkInterface.create(:veid=>veid, :name=>name)
  end


end
