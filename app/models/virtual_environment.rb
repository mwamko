# Mwamko  Copyright (C) 2007  Aleksandr O. Levchuk

class VirtualEnvironment < ActiveRecord::Base

  ## Let's not deal with this for now
  # belongs_to :user, {:foreign_key => 'uid'}
  # has_one :user

  validates_presence_of(:veid, :uid, :status, :updated_on)
  validates_numericality_of(:veid, :uid, :only_integer => true)
  validates_uniqueness_of(:veid)
  # validates_uniqueness_of(:ip) but allow it to be nil

  # Safety Checks
  # The tests for SAFETY should be done before validation
  # because functions like "validates_uniqueness_of" send
  # the strings to outside (out to the DB) while they are
  # invalid.

  def before_validation
    if veid.to_s !~ SAFE_VEID
      write_attribute(:veid, 'veid is not safe') # Rewrite the :veid variable
    end
  end

  # Take over the failed SAFETY Checks
  validates_exclusion_of :veid,
                         :in => ['veid is not safe'],
                         :message => "should be a valid VEID"



  def status
    super
  end

  def all_possible
    out = []
    IP_RANGE.to_a.each do |ip_end|
      out <<
        (IP_BEGINNING.to_s + ip_end.to_s)
    end
    out
  end

  def all_possible_local
    out = []
    LOCAL_IP_RANGE.to_a.each do |ip_end|
      out <<
        (LOCAL_IP_BEGINNING.to_s + ip_end.to_s)
    end
    out
  end

  def grab_available_ip

    # This needs to be an atomic transaction
    VirtualEnvironment.transaction do

      ip_occupiers =
        VirtualEnvironment.find_all_by_status(['started',
                                              'stopping',
                                              'starting'])

      current_ips = ip_occupiers.collect do | v |
        v.ip
      end

      all_available = all_possible.collect do |ip|
        ip unless current_ips.include?(ip)
      end

      all_available.compact!

      if all_available.empty?
        raise "No more IPs"
      else
        self.ip = all_available[0]
        self.save!
      end
    end
  end

  def grab_available_local_ip

    # This needs to be an atomic transaction
    VirtualEnvironment.transaction do


      ip_occupiers =
        VirtualEnvironment.find_all
                                   #_by_status(['started',
                                   #            'stopped',
                                   #            'stopping',
                                   #            'starting',
                                   #            'destroying'])

      current_ips = ip_occupiers.collect do |v|
        LocalIP.find_all_by_veid(v.veid).collect do |i|
          i.ip
        end
      end
      current_ips.flatten!

      all_available = all_possible_local.collect do |ip|
        ip unless current_ips.include?(ip)
      end

      all_available.compact!

      if all_available.empty?
        raise "No more IPs"
      else
        ip = all_available[0]
        lip = LocalIP.new
        lip.veid = self.veid
        lip.ip = ip
        lip.save!
      end

      return ip
    end
  end

  def free_ip
  end

  def grab_available_veid
    next_ve =
      VirtualEnvironment.find_by_uid(self.uid, :order => "veid DESC")

    if next_ve.nil?
      self.veid = "#{self.uid}01".to_i
      return self.veid
    end

    raise "No more VE Identifiers" if next_ve.veid.to_s[-2..-1] == '00'
    self.veid = next_ve.veid + 1
    return self.veid
  end


  protected

  def validate

    # The Only Way
    preset_values = ['starting', 'started',
                     'creating', 'stopping', 'stopped',
                     'destroying', 'destroyed',
                     'mounted']

    errors.add(:status, "should be one of the preset values " +
               preset_values.inspect) unless
      preset_values.include?(status)

    # A more descriptive error, but it will make unit tests fail
    ## errors.add(:veid, "(#{veid}) should begin with the uid (#{uid})") unless
    errors.add(:veid, "should begin with the uid") unless
      veid.to_s =~ /^#{uid}([[:digit:]]{2})$/

    errors.add(:uid, "should be positive") if uid.nil? || uid <= 0

    errors.add(:status, "should not be nil") if status.nil?

    errors.add(:ip, "should be within the configured range") unless
      all_possible.include?(ip) || ip.nil?

    errors.add(:setup_needed, "should be boolean") unless
      [TrueClass, FalseClass].include? setup_needed.class

    errors.add(:uid, ["#{uid} should be an exsting user.",
                      "VE #{veid} cannot be added.",
                     ""].join(" ")) unless
      User.exists?(uid)
  end


end
