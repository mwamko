class OpenvzServer < ActiveRecord::Base

  def self.selected_server
    all_hosts = OpenvzServer.find(:all)
    raise NO_OPENVZ_SERVER_ERROR if all_hosts.empty?
    host = all_hosts.last.ip
  end

  private

  def validate
    if ip.nil? 
      errors.add(:ip,"ip, should not be nil")
    elsif ip =~ /^(\d\d?\d?)\.(\d\d?\d?)\.(\d\d?\d?)\.(\d\d?\d?)$/
      errors.add(:ip,"part of IP is out of 255 range") if
      ( $1.to_i>255 or $2.to_i>255 or $3.to_i>255 or $4.to_i>255 )
    elsif ip =~ /\A([a-fA-F0-9]{4}:){7}([a-fA-F0-9]{4})\Z/ 
      errors.add(:ip,"ipv6 not supported")
    else
      errors.add(:ip,"bad format")
    end
  end

end
