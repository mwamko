class NewStudent < ActionMailer::Base

  def login_info(email, username, new_password)
    recipients email
    from       '"Aleksandr Levhuk" <alevchuk@gmail.com>'
    cc         '"Aleksandr Levhuk" <alevchuk@gmail.com>'

    subject    "Mwamko: New Student Account"
    body       :username => username, :new_password => new_password
  end

   def reset_password(email, username, email_token)
    recipients email
    from       '"Miguel Ziranhua" <miguel013@gmail.com>'
    cc         '"Miguel Ziranhua" <miguel013@gmail.com>'

    subject    "Mwamko: Reset Password Request"
    body       :username => username, :email_token => email_token
  end


  def new_password(email, username, new_password)
    recipients email
    from       '"Miguel Ziranhua" <miguel013@gmail.com>'
    cc         '"Miguel Ziranhua" <miguel013@gmail.com>'

    subject    "Mwamko: New Password"
    body       :username => username, :new_password => new_password
  end


end
