class List

  def initialize
    @raw_data = `#{SUDO} #{VZTOOLS_PATH}/vzlist -a -H -o veid,status,ip`
  end

  def each
    lines = @raw_data.chomp.split("\n")
    
    lines.each do |line|
      id, status, ip_addr = line.chomp.split
      yield VE.new(id, status, ip_addr)
    end
  end

  def items
    @items = []
    each {|i| @items << i}
    
    @items
  end
end


class VE
  
  def initialize (id, status, ip_addr)
    @id = id
    @status = status
    @ip_addr = ip_addr

    @db_info = VirtualEnvironment.find_by_veid(id) 
  end

  def db_info
    @db_info
  end

  def id
    @id
  end

  # This function is a hack
  # Not well tested
  def inconsistent?

    # @status is what OpenVZ is telling us 
    # it can be only 'started','stopped', or 'mounted'

    return true if @db_info == nil
    

    if ['started','stopped','mounted'].include? @db_info.status
      if @db_info.status == @status
        return false # Consistent
      else 
        return true # Not Consistent
      end
    end

    return true if @status == 'stopped' && 
      @db_info.status != 'starting' && @db_info.status != 'destroying' 
    return true if @status == 'started' && @db_info.status != 'stopping'

    return false
  end
  
  def status
    return nil if inconsistent?
    return @db_info.status
  end

  def ip_addr
    @ip_addr
  end

end
