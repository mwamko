# creates the Menu object which keeps track of product displays
# such as the state of the description (open or closed)
class Menu

  attr :item_state
  
  def initialize
    #The box state tells us if the box for a particular product is opened
    #or
    #closed.  It is a hash whose key/value is product/box_state.
    @item_state = Hash[]
    
    items = [:diskspace, :memory, :uptime]
    #initialize product box states to closed
    items.each do |i|
      @item_state[i] = 'opened'
    end
  end
end
