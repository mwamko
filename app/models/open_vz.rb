class String
  def pretty_line(symbol='-')
    center(80, symbol)
  end
end

class OpenVZ

  def self.list(*fields)    

    fields.collect! {|i| i.to_sym}
    
    ## Do not raise here,
    ## Let the usage infomation from the command line be raised instead.
    # raise "Please provide the fields that you want to list." if fields.empty?

    raise ["This function does not support the ip field yet,",
           "because it is hard to parse.",
           "",
           "See: http://bugzilla.openvz.org/show_bug.cgi?id=686",
           ""].join("\n") if fields.include? :ip

    remote_cmd = [
                  "#{REMOTE_SUDO}",
                  "#{REMOTE_VZTOOLS_PATH}/vzlist -a --no-header",
                  "-o #{fields.join(',')}"
                 ].join("\\\n ")

    cmd        = TO_SSH.call(remote_cmd, OpenvzServer.selected_server)
    result    = RUN4(cmd)
    
    if !(result.errors || '').strip.empty?
      raise "vzlist produced errors\n" + result[:errors]
    end
        
    if result.out.nil?
      raise ["vzlist STDOUT stream is nil",
             cmd
            ].join("\n")
    end

    lines = result.out.split("\n")
    return lines.collect do |line|
      row = line.split(' ')
      if row.size != fields.size
        raise ["Unexpected line from vzlist",
               line.inspect,
               row.inspect,
               "I was expecting the number of elements to be #{fields.size}"
              ].join("\n")
      end

      hashed_row = Struct.new(*fields).new
      row.reverse! # cuz I be poppin'
      fields.each do |col| 
        value = row.pop

        # Convert to Integer if you can safely do so,
        # if not then leave as string but strip the white spaces
        value = (Integer(value) rescue value.strip)
        
        # Subtle OpenVZ/Mwamko differences
        value = 'started' if value == 'running' 

        hashed_row[col] = value
      end
      next hashed_row
    end
  end
  


  require 'open3' # TODO: Replace with RUN4

  # STDOUT and STDERR logging
  def self.`(cmd) # ` emacs fix

    lopenvz_log_file = File.new("#{RAILS_ROOT}/log/open_vz.log", 
                                File::CREAT|File::APPEND|File::WRONLY)
       

    system_call_header = Time.now.to_s.pretty_line("=")

    data = []
    data << system_call_header
    data << 'Command and Arguments'.pretty_line
    data << cmd
    lopenvz_log_file << data.join("\n") << "\n"
    lopenvz_log_file.flush


    i, o, e = Open3.popen3(cmd)
    
    output = o.entries.join
    errors = e.entries.join

    data = []
    data <<  system_call_header
    data << 'STDERR'.pretty_line
    data << errors   

    data << 'STDOUT'.pretty_line
    data << output
    data << "\n"
    lopenvz_log_file << data.join("\n") << "\n"
    lopenvz_log_file.close

    return "#{output}" ##{errors}"
  end

  def self.df (veid)
    
    raise "Wrong #{veid.inspect}" unless 
     (veid.to_s =~ SAFE_VEID)

    cmd=`#{SUDO} #{REMOTE_VZTOOLS_PATH}/vzctl exec #{veid} df`
    return cmd

  end
 
  def self.upgrade(veid)
    raise "Wrong #{veid.inspect}" unless 
      (veid.to_s =~ SAFE_VEID)
    
    out =`#{SUDO} #{VZTOOLS_PATH}/vzctl exec #{veid} apt-get dist-upgrade -y`
    return out
  end

  def self.uptime (veid)

    raise "Wrong #{veid.inspect}" unless 
      (veid.to_s =~ SAFE_VEID)

    cmd=`#{SUDO} #{REMOTE_VZTOOLS_PATH}/vzctl exec #{veid} uptime`
    return cmd
  end

  def self.rsa_fingerprint (veid)
    raise "Wrong #{veid.inspect}" unless 
      (veid.to_s =~ SAFE_VEID)
    
    x_fingerprint('rsa', veid)
  end

  def self.dsa_fingerprint (veid)

   raise "Wrong #{veid.inspect}" unless 
     (veid.to_s =~ SAFE_VEID)

    x_fingerprint('dsa', veid)
  end
 

  
  def self.remove_ip_from_ve(veid)

    raise "Wrong #{veid.inspect}" unless 
      (veid.to_s =~ SAFE_VEID)

    ve = VirtualEnvironment.find_by_veid(veid)
    `#{SUDO} #{REMOTE_VZTOOLS_PATH}/vzctl set #{veid} --ipdel all --save`



    # This is very ugly. AjaxTerm server should be re-implemented.

    # The cleanup of the know_host file is handled here, 
    # but the file is created from 
    # ./script/ajaxterm_setup_files/username_for_ssh

    `rm ~/.ssh/#{ve.ip}_known_host`    



  end
  
  
  def self.add_user_to_ve_sudoers(uname, veid)

    raise "Wrong #{veid.inspect}" unless 
      (veid.to_s =~ SAFE_VEID)

    cmd = %Q!printf echo\\ #{uname}\\ ALL=\\(ALL\\)\\ ALL\\ \\>\\>\\ /vz/private/#{veid}/etc/sudoers | #{SUDO} #{SSH} -T #{OpenvzServer.find(:all).last.ip}!


    `#{cmd}`

  end

  def self.set_ip_in_ve(ve)

    raise "Wrong #{ve.veid.inspect}" unless 
      (ve.veid.to_s =~ SAFE_VEID)

    cmd = "#{SUDO} #{REMOTE_VZTOOLS_PATH}/vzctl set #{ve.veid} --ipadd " +
      "#{ve.ip} --save"

    `#{cmd}`
  end

  def self.add_local_ip(veid, ip)

    raise "Wrong #{veid.inspect}" unless 
      (veid.to_s =~ SAFE_VEID)

    raise "Wrong #{ip.inspect}" unless 
      (ip.to_s =~ SAFE_IP4)

    cmd = "#{SUDO} #{REMOTE_VZTOOLS_PATH}/vzctl set #{veid} --ipadd #{ip} --save"
    `#{cmd}`
  end

  def self.add_netif(veid, name)

    raise "Wrong #{veid.inspect}" unless 
      (veid.to_s =~ SAFE_VEID)

    unless (name.to_s =~ SAFE_NETIF_NAME)
      raise "Choose the network interface name to be " +
        "lower-case English letters and digits only. \n" +
        "#{name.inspect} was not a name like that."
    end

    cmd = 
      "#{SUDO} #{REMOTE_VZTOOLS_PATH}/vzctl set #{veid} --netif_add #{name} --save"

    out = `#{cmd} 2>&1 1>/dev/null`

    if out.strip != ''
      raise out
    end
  end

  def self.remove_local_ip(veid, ip)

    raise "Wrong #{veid.inspect}" unless 
      (veid.to_s =~ SAFE_VEID)

    raise "Wrong #{ip.inspect}" unless 
      (ip.to_s =~ SAFE_IP4)

    cmd = "#{SUDO} #{REMOTE_VZTOOLS_PATH}/vzctl set #{veid} --ipdel #{ip} --save"
    `#{cmd}`
  end 


  def self.set_ve_hostname(ve)

    raise "Wrong #{ve.veid.inspect}" unless 
      (ve.veid.to_s =~ SAFE_VEID)

    cmd = "#{SUDO} #{REMOTE_VZTOOLS_PATH}/vzctl set " +
      "#{ve.veid} --hostname #{ve.veid} --save"

    `#{cmd}`
  end
 
  def self.ve_DNS_config(veid)

    raise "Wrong #{veid.inspect}" unless 
      (veid.to_s =~ SAFE_VEID)
 
    cmd = []
    cmd << "#{SUDO} #{REMOTE_VZTOOLS_PATH}/vzctl set #{veid}"
    cmd <<  "--searchdomain #{DNS_SEARCHDOMAIN}"
    DNS_SERVER.each do |i|
      cmd << "--nameserver #{i}"
    end
 
    cmd << "--save" 
    `#{cmd.join(' ')}`
  end               
  
  def self.set_ve_userpassw(veid, uname, upass)
 
    raise "Wrong #{veid.inspect}, #{uname.inspect}, or #{upass.inspect}" unless
      (veid.to_s =~ SAFE_VEID and 
       uname =~ SAFE_EMAIL and 
       upass =~ SAFE_PASSWORD)
 
    cmd = "#{SUDO} #{REMOTE_VZTOOLS_PATH}/vzctl set #{veid} " +
      "--userpassw #{uname}:#{upass}"
    
    `#{cmd}`
  end

  def self.ssh_keygen(veid)
    raise "Wrong #{veid.inspect}" unless (veid.to_s =~ SAFE_VEID)

    cmd_start = "#{SUDO} #{REMOTE_VZTOOLS_PATH}/vzctl exec #{veid}"

    `#{cmd_start} rm /etc/ssh/ssh_host_rsa_key`
    `#{cmd_start} rm /etc/ssh/ssh_host_rsa_key.pub`
    `#{cmd_start} #{SSH}-keygen -q -t rsa -f /etc/ssh/ssh_host_rsa_key`

    `#{cmd_start} rm /etc/ssh/ssh_host_dsa_key`
    `#{cmd_start} rm /etc/ssh/ssh_host_dsa_key.pub`
    `#{cmd_start} #{SSH}-keygen -q -t dsa -f /etc/ssh/ssh_host_dsa_key`
  end


  def self.list_environments
    ve_list = `#{SUDO} #{REMOTE_VZTOOLS_PATH}/vzlist -a -o veid --no-header`.split
  end



  def self.list_users(veid)
    # Users that Have Passwords

    raise "Wrong VEID #{veid.inspect}" unless 
      (veid.to_s =~ SAFE_VEID)

    output_and_errors = 
      `#{SUDO} #{REMOTE_VZTOOLS_PATH}/vzctl exec #{veid} cat /etc/shadow`

    raise "No Shadow File" if output_and_errors.empty?

    names_and_passwords = output_and_errors.split("\n").collect do |line|
      items = line.split(':')
      [items[0], items[1]]
    end

    names_and_passwords.inject([]) do |bucket, i|
      if i[1].size > 2 and i[0] != "root"
        bucket << i[0]
      else
        bucket
      end
    end
  end

  def self.ls(veid, path)
    cmd_start = "#{SUDO} #{REMOTE_VZTOOLS_PATH}/vzctl exec #{veid}"      
    `echo \"#{cmd_start} ls -1 #{path}\" | #{SSH} -T #{OpenvzServer.find(:all).last.ip}`.collect{|f| f.chomp}
  end

  def self.template_size(template)
    host = OpenvzServer.find(:all).last.ip
    cmd = 
      "echo \"#{GZIP} --quiet --list /vz/template/cache/#{template}.tar.gz\" | 
      #{SSH} -T -i #{RAILS_ROOT}/tmp/mwamko_id_rsa #{host}"
    `#{cmd}`.split[0..1]
  end

# Methods from application controller and nucleus controller  
private
  def self.x_fingerprint(x, veid)

    raise "Wrong #{veid.inspect}" unless
      (veid.to_s =~ SAFE_VEID)

    pos = ['rsa', 'dsa']

    raise "Should be #{pos.join(" or ")} only." unless pos.include?(x)
    
    cmd=[]
    cmd << "#{SUDO} #{REMOTE_VZTOOLS_PATH}/vzctl exec #{veid}"
    cmd << "#{SSH}-keygen -lf /etc/ssh/ssh_host_#{x}_key"

    r = /\d+ (([a-f0-9]{2}:)*[a-f0-9]{2}) \/etc/
    return nil unless `#{cmd.join(' ')}` =~ r
    return $1
  end
end
