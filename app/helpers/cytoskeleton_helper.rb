module CytoskeletonHelper

  def area_controls (area)

    def button_args
      {:name => 'button', 
        :id => 'area_button',
        :onclick => "stop_polling=true"}
    end

    case area
    when 'templates' 
      return submit_tag("Create", button_args) 
    
    when 'stopped'
      return submit_tag('Start', button_args)
      
    when 'started'
      return submit_tag('Stop', button_args) # + 
                                          # submit_tag('Upgrade', button_args)

    when 'destroy'
      return submit_tag("Destroy", button_args)

    else
      ""
    end
  end

  def display_area(area_type, items_html, options={})

    out = []
    out << "<form action=\"#{url_for(:controller => 'nucleus')}\"" +
         " onsubmit=\"return confirmUserAction('#{area_type}')\"" +
         " method=\"post\">"
    out << content_tag("h1", area_type.capitalize)

    if items_html.strip == '' 
      out << '<div>There are no items here.</div>'
    else
      out << items_html      
      out << area_controls(area_type) unless options[:display_controls] == true
    end

    out << end_form_tag

    return out.join("\n")
  end
end
