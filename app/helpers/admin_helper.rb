module AdminHelper

  def sortable_column_name(c, human_name)
    '<td class="column_name">' + 
      if column_is_ordered(c)
        '<img class="column_name_background" 
        src="/images/admin/table_header_selected.png"/>'
      else
        '<img class="column_name_background" 
        src="/images/admin/table_header.png"/>'
      end +
      '<span>&nbsp;&nbsp;&nbsp;</span><span 
           class="column_name_foreground">' + 
      human_name +
      '</span>' +
      if column_is_ordered_up(c)
         link_to_different_order('<img 
           class="order_arrow_image" border="0" 
           src="/images/admin/table_header_selected_up.png"/>',
           [c] + @order_list[1..-1],
           ['DESC'] + @order_directions[1..-1])

      elsif column_is_ordered_down(c)
         link_to_different_order('<img 
           class="order_arrow_image" border="0" 
           src="/images/admin/table_header_selected_down.png"/>',
           [c] + @order_list[1..-1],
           ['ASC'] + @order_directions[1..-1])

      elsif column_is_down(c)
         link_to_different_order('<img 
         class="order_arrow_image" border="0" 
         src="/images/admin/table_header_down.png"/>',
           [c] + @order_list,
           ['DESC'] + @order_directions)

      else
         link_to_different_order('<img 
         class="order_arrow_image" border="0" 
         src="/images/admin/table_header_up.png"/>',
           [c] + @order_list,
           ['ASC'] + @order_directions)
      end + 
      '</td>'
  end

  def column_is_ordered(column_name)
    column_is_ordered_up(column_name) or
      column_is_ordered_down(column_name)
  end


  def column_is_ordered_up(column_name)
    column_is_ordered_X(column_name, 'ASC')
  end

  def column_is_ordered_down(column_name)
    column_is_ordered_X(column_name, 'DESC')
  end

  def column_is_down(column_name)
    if !@order_list.nil? and !@order_directions.nil?
      if @order_list.size != @order_directions.size
        raise "@order_list=#{@order_list.inspect} and " +
          "@order_directions=#{@order_directions.inspect} of different sizes"
      end
      t=[]
      @order_list.each_with_index do |c, i|
        # Element must not be first         
        if i != 0 and c == column_name
          return {'DESC'=>true, 'ASC'=>false}[@order_directions[i]]
        end
      end
    end
    return false
  end

  private

  def column_is_ordered_X(column_name, x)
    if !@order_list.nil? and @order_list.size > 0 and 
        column_name == @order_list.first and
        
        !@order_directions.nil? and @order_directions.size > 0 and
        @order_directions.first == x

      return true
    end
  end

  def link_to_different_order(contents, new_order_list, new_order_directions)

    new_order_list.delete([])
    new_order_directions.delete([])

    '<a class="order_arrow" href="?' +
         "order_list=#{new_order_list.join('/')}&" +
         "order_directions=#{new_order_directions.join('/')}" +
         '"' +
         ">#{contents}</a>"

  end
end
