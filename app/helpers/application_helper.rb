# Methods in this helper will be available to all templates in the application.
require 'ansi2html'

module ApplicationHelper

  def no_wrap(s)
    content_tag 'div', s, :style=>'white-space: nowrap;'
  end


  def back
    '<a href="javascript:history.back()">Back</a>'
  end

  def simple_message(msg)
    "<p>#{msg}</p>\n<p>#{back}</p>"
  end


  def unicode_CHECK_MARK
    '&#10003;'
  end
   
  def unicode_BALLOT_X
    '&#10008;'
  end
  
  def unicode_CIRCLE_ARROW
    '&#10162;'
  end

  def unicode_HEART
    '&#x2764;'
  end

  def unicode_SKULL
    '&#x2620;'
  end
  
  def footer_line
    revision = 0 #Development.revision
    date = Time.now - 10.minutes.ago  #Development.date
    author = 'alevchuk' #Development.author

    '<div id="footer">
       <pre style="width: 725px;">' +
      "\n" +
      "<b>Terms of Use:</b>\n\n" +
      "Mwamko is a Free Software. " +
      '<a href="http://www.fsf.org/licensing/essays/free-sw.html">'+
      "More...</a>\n" +
      "\n\n" +
      '<b>Development Information:</b>' +
      "\n\n" +
      Ansi2Html.convert(h(Development.git_log_short)) +
      "\n" +
      link_to('More...', :controller=>'development') +
      '</pre>

       <a href="http://mwamko.org">Mwamko</a> is a project started in the
       <a href="http://www.cs.ucr.edu/"
       style="white-space: nowrap">Department of 
       Computer Science</a> at the 
       <a href="http://www.ucr.edu" 
       style="white-space: nowrap">University of California, Riverside</a> in 2006.
     </div>
    '
  end
end
