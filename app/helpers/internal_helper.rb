module InternalHelper

  def print_members (the_object, members)
    ret = ''
    members.each do |member|
      ret = ret + "
  <div><span
  id=\"ruby_method\">#{the_object.class.to_s}##{member.to_s}</span>
  &#10143;
  #{the_object.method(member).call}</div>
    "
    end
    ret
  end
end
