#Mwamko  Copyright (C) 2007  Patrick M. Staight
require File.dirname(__FILE__) + '/../test_helper'

class UserTest < Test::Unit::TestCase
  fixtures :users

  def test_find_user_by_id
    users_form_db = User.find(:all)
    assert_equal User.find_user_by_id(users_form_db, 1), users(:alex)
    assert_equal User.find_user_by_id(users_form_db, 2), users(:miguel)
    assert_equal User.find_user_by_id(users_form_db, 3), users(:chong)
    assert_raise(RuntimeError) do
      User.find_user_by_id(users_form_db, 4)
    end
  end
  #tests basic function
  def test_destroy_pg_err_email_username
    a=User.create(:name => "first@gmail.com")
    assert a.valid?
    assert_nothing_raised ActiveRecord::StatementInvalid do
      a.destroy
    end
  end

  #tests for double username bug
  def test_add_two_users
    a=User.create(:name => "user1@gmail.com")
    b=User.create(:name => "user2@gmail.com")
    assert a.valid?
    assert b.valid?
    assert_nothing_raised ActiveRecord::StatementInvalid do
      a.destroy
      b.destroy
    end
  end
  #passes nil for username
  def test_empty_username
    a=User.create()
    assert a.valid? == false
    b=User.create(:name => "")
    assert b.valid? == false
    c=User.create(:name => "@gmail.com")
    assert c.valid? == false
    assert_nothing_raised ActiveRecord::StatementInvalid do
      a.destroy
      b.destroy
      c.destroy
    end
  end
  #tests that usernames are made only of email address chars
  def test_wanky_username
    illeagal =  ['/'[0],'`'[0]]
    (     1..'-'[0]).each { |i| illeagal << i }
    (':'[0]..'@'[0]).each { |i| illeagal << i }
    ('['[0]..'^'[0]).each { |i| illeagal << i }
    ('{'[0]..255   ).each { |i| illeagal << i }
    illeagal.delete '+'[0]
    illeagal.each do |i|
      crazy_name="bad_char" + i.chr + "@gmail.com"
      #p crazy_name
      assert_nothing_raised ActiveRecord::StatementInvalid do
        a=User.create(:name => crazy_name)
        assert !a.valid?, crazy_name + " should not be allowed as email" 
        a.destroy
      end
    end
    illeagal.delete '-'[0]
    illeagal << '_'[0] << '+'[0]
    illeagal.each do |i|
      crazy_name="user@bad" + i.chr + "host.com"
      assert_nothing_raised ActiveRecord::StatementInvalid do
        a=User.create(:name => crazy_name)
        assert !a.valid?, crazy_name + " should not be allowed as email"
        a.destroy
      end
    end
  end
  #makes a user with a non-matching password, skipping destroy
  def test_paswd_mismatch
    a=User.create(:name => "user1@hoast.com", :password => '12345678', :password_confirmation => '87654321')
    assert a.valid? == false
  end
  def test_paswd_too_short
    a=User.create(:name => "user2@hoast.com", :password => '1234567', :password_confirmation => '1234567')
    assert a.valid? == false
  end
  def test_paswd_correct_entry
    a=User.create(:name => "user3@hoast.com", :password => '12345678', :password_confirmation => '12345678')
    assert a.valid?
  end
end
