# Mwamko  Copyright (C) 2007  Patrick M. Staight
require File.dirname(__FILE__) + '/../test_helper'

class VirtualEnvironmentTest < Test::Unit::TestCase
  fixtures :virtual_environments

  def test_invalid_with_empty_attributes
    ve = VirtualEnvironment.new
    assert !ve.valid?
    assert ve.errors.invalid?(:veid)
    assert ve.errors.invalid?(:uid)
    assert ve.errors.invalid?(:status)
    assert ve.errors.invalid?(:updated_on)

    assert_equal ve.setup_needed.class, TrueClass
  end

  def test_positive_uid
    ve = VirtualEnvironment.new(:veid        => '191',
                                :status      => 'creating',
                                :updated_on => Time.now)

    ve.uid = -1
    assert !ve.valid?
    assert_equal 'should be positive', ve.errors.on(:uid)

    ve.uid = 0
    assert !ve.valid?
    assert_equal 'should be positive', ve.errors.on(:uid)

    ve.uid = 1
    assert ve.valid?
  end

  def test_matching_uid_and_veid
    ok = [[732,7], 
          [999,9],
          [1901,19], 
          [999801,9998],
          [9_999_999_999_99,9_999_999_999]]

    bad = [[103,4], 
           [999,8],
           [1901,1], 
           [999801,9999],
           [9_999_999_999_99,10_000_000_000]]

    ok.each do |veid, uid|
      ve = VirtualEnvironment.new(:veid        => veid,
                                  :uid         => uid,
                                  :status      => 'creating',
                                  :updated_on  => Time.now)

      assert ve.valid?, ve.errors.full_messages.inspect
    end

    bad.each do |veid, uid|
      ve = VirtualEnvironment.new(:veid        => veid,
                                  :uid         => uid,
                                  :status      => 'creating',
                                  :updated_on  => Time.now)

      assert !ve.valid?, 'giving VE#{veid} to user #{uid}'
      assert_equal 'should begin with the uid', ve.errors.on(:veid)
    end
  end

  def test_veids_less_than_100
    (0..99).each do |i|
      ve = VirtualEnvironment.new(:veid        => i.to_s,
                                  :uid         => i/100,
                                  :status      => 'creating',
                                  :updated_on  => Time.now)
      if !ve.valid? then
        ve.errors.full_messages.inspect.each do |msg| 
          assert msg !~ /has already been taken/,
           "Veid #{i} exists and is less than 100"
        end
      else
        assert false,"Created veid #{i} which is less than 100"
      end
    end
  end
  
  def test_duplacate_IP
    ve1= VirtualEnvironment.new(:veid        => '192',
                                :uid         => 1,
                                :status      => 'creating',
                                :updated_on  => Time.now)
    ve1.grab_available_ip
    ve1.status = 'starting'
    assert ve1.valid?
    ve1.status = 'started'
    assert ve1.valid?
    ve1.status = 'stopping'
    assert ve1.valid?
    ve1.status = 'stopped'
    assert ve1.valid?
  end
end
