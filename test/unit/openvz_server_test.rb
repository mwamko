require File.dirname(__FILE__) + '/../test_helper'

class OpenvzServerTest < Test::Unit::TestCase
  fixtures :openvz_servers

  def test_good_ip
    a=OpenvzServer.create(:ip => "10.0.0.1")
    assert_valid a
  end
  def test_ipv6_unsupported
    a=OpenvzServer.create(:ip => '2001:0db8:85a3:08d3:1319:8a2e:0370:7334')
    assert !a.valid?
  end
  def test_bad_ip
    a=OpenvzServer.create(:ip => 'not even close')
    assert !a.valid?
    a=OpenvzServer.create(:ip => 'it.has.3.dot')
    assert !a.valid?
    a=OpenvzServer.create(:ip => '10.0.0.256')
    assert !a.valid?
  end
end
