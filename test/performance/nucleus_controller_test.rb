require File.dirname(__FILE__) + '/../test_helper'
require 'nucleus_controller'

# Re-raise errors caught by the controller.
class NucleusController; def rescue_action(e) raise e end; end

class NucleusControllerTest < Test::Unit::TestCase

  self.fixture_path = 
    File.join(File.dirname(__FILE__), "../fixtures/performance" )

  fixtures :users


  def setup
    @controller = NucleusController.new
    @request    = ActionController::TestRequest.new
    @response   = ActionController::TestResponse.new
  end


  def test_create_a_lot_of_ves
    @controller.logger.silence do
      elapsed_time = Benchmark.realtime do
        10_000.upto(10_005) do |i| 
          @request.session[:user_id] = i
          post 'create', :template => 'ubuntu-dapper.with-rails'          
        end
      end
    end

  end
end
