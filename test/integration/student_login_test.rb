require "#{File.dirname(__FILE__)}/../test_helper"

class StudentLoginTest < ActionController::IntegrationTest
  # fixtures :your, :models
  fixtures :users


  def test_index_without_user
    post 'login/login'
    assert_redirected_to :controller => 'homepage'
    assert_equal 'Invalid user/password combination', flash[:notices][0]
  end 


  def test_alex_login
    alex = users(:alex)
    post_via_redirect('login/login', 
                      :name => alex.name, 
                      :password => 'Alex\'s Secret')

    assert_equal alex.id, session[:user_id] 
    
    assert_response (:success, 
                     "could not get to cytoskeleton/areas after logging in\n" +
                     request.parameters.inspect)

    assert_template "cytoskeleton/areas"
  end

  def test_bad_password
    alex = users(:alex)
    post 'login/login', :name => alex.name, :password => 'Alex\'s not Secret'
    assert_redirected_to :controller => 'homepage'
    assert_equal 'Invalid user/password combination', flash[:notices][0]
  end
end
