require File.dirname(__FILE__) + '/../test_helper'
require 'init_page_controller'

# Re-raise errors caught by the controller.
class InitPageController; def rescue_action(e) raise e end; end

class InitPageControllerTest < Test::Unit::TestCase
  def setup
    @controller = InitPageController.new
    @request    = ActionController::TestRequest.new
    @response   = ActionController::TestResponse.new
  end

  # Replace this with your real tests.
  def test_add_server
    get :index
    assert :success

    post :validate_key, :ip=>'2123'
    assert_redirected_to :action => "index"
    assert_equal "2123 is not a valid ip" , flash[:error]

    post :validate_key, :ip=>'208.109.170.201'
    assert :success

    post(:add_openvz_server, 
         :ip=>'208.109.170.201', 
         :responce=>'yes',
         :new_key=> "208.109.170.201 ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAIEA62dhBmeFOE6aOGQQ2Pjg1KLuvZ8v3ZCtOhKbNhRk/YE5u7ckkzTsAFRJ1Dc2qnmh296OsfMgUgcXLcgSe7OJiT+WdRH55mfldhKlUXBf3vtG4HT4eaC9YSNDWGU09Ec+rohbzYdwNPWedf3dyPKirOzho47iDWsP8OwXxIqdLrs=\n",
         :fp=>"1024 bb:88:6d:20:d2:a9:6b:3e:0e:9e:" +
         "67:83:c8:ed:ed:60 208.109.170.201\n")
    assert_redirected_to :action => "index"
    assert_nil flash[:error]
    assert_equal OpenvzServer.find(:all).last.ip, '208.109.170.201'
  end

  def test_empty_spaces
    get :index
    assert :success

    post :validate_key, :ip=>' 208.109.170.202'
    assert :success

    post(:add_openvz_server, 
         :ip=>' 208.109.170.202', 
         :responce=>'yes',
         :new_key=> "208.109.170.202 ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAIEA62dhBmeFOE6aOGQQ2Pjg1KLuvZ8v3ZCtOhKbNhRk/YE5u7ckkzTsAFRJ1Dc2qnmh296OsfMgUgcXLcgSe7OJiT+WdRH55mfldhKlUXBf3vtG4HT4eaC9YSNDWGU09Ec+rohbzYdwNPWedf3dyPKirOzho47iDWsP8OwXxIqdLrs=\n",
         :fp=>"1024 bb:88:6d:20:d2:a9:6b:3e:0e:9e:67:83:c8:ed:ed:60 208.109.170.202\n")
    assert_redirected_to :action => "index"
    assert_nil flash[:error]
    assert !OpenvzServer.find(:all).empty?
    assert_equal OpenvzServer.find(:all).last.ip, '208.109.170.202'

  end
end
