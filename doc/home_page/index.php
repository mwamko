<?
include_once "markdown.php";

function open($filename) {
  $handle = fopen($filename, 'r');
  return fread($handle, filesize($filename));
}

$title = "Mwamko's Home Page";
$team_meeting_overview = Markdown(open('team_meeting_overview.txt'));

$heading = open('heading.html');
$heading = str_replace('%(title)s', $title, $heading);

$google_calendar = open('google_calendar.html');

$footer = open('footer.html');

$page = open('template.html');
$page = str_replace('%(title)s', $title, $page);
$page = str_replace('%(body)s',  $team_meeting_overview,  $page);

echo $heading;
echo $google_calendar;
echo $page;
echo $footer;

?>