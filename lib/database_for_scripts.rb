#    Mwamko  Copyright (C) 2007  Aleksandr O. Levchuk
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

# This only loads mwamko constants and models
# Works faster then loading the entire Rails environment

# Used by command line tools which do not need ActionController, etc...

module DatabaseForScripts
  SCRIPT_DIR = File.dirname(File.expand_path(__FILE__))
  SCRIPT_NAME = $0.split('/')[-1]
end

require File.expand_path("#{DatabaseForScripts::SCRIPT_DIR}/../" +
                         "config/mwamko_constants")
require File.expand_path("#{DatabaseForScripts::SCRIPT_DIR}/" +
                         "../vendor/rails/activerecord/lib/active_record")
require 'erb'
require 'yaml'

#RAILS_CONNECTION_ADAPTERS = ["mysql", "postgresql", "sqlite", 
#                             "firebird", "sqlserver", "db2", 
#                             "oracle", "sybase", "openbase", "frontbase"]

database_configuration_file = "#{RAILS_ROOT}/config/database.yml"
database_configurations = 
  YAML::load(ERB.new(IO.read(database_configuration_file)).result)
database_configuration = database_configurations[ENV['RAILS_ENV']]
ActiveRecord::Base.establish_connection(database_configuration)

# real    0m0.624s

$LOAD_PATH << "#{RAILS_ROOT}/lib"

model_files = Dir.glob("#{RAILS_ROOT}/app/models/*.rb")
model_files.each do |file| 
  begin
    require file
  rescue NameError => detail
    case detail.to_s
    when 'uninitialized constant ActionMailer'
      # Ignore ActionMailer models
    else
      raise 
    end
  end
end

# real    0m0.659s

ReEstablishDatabaseConnection = Proc.new do
  database_configuration = ActiveRecord::Base.remove_connection
  ActiveRecord::Base.establish_connection(database_configuration)
end
