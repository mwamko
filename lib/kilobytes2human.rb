class Kilobytes2Human
  def self.convert( kilobytes )
    
    conversion_table = {
      1.0 => 'Kb',
      1024.0 => 'Mb',
      1024.0**2 => 'Gb'
    }

    out = 0.0
    conversion_table.each_pair do |key, val|
      if kilobytes > key - 1 && kilobytes <= key ** 2
        out = (kilobytes / key).round
        out = out.to_s << ' ' << val
      end
    end

    out
  end
end

