#    Mwamko  Copyright (C) 2007  Aleksandr O. Levchuk
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

module CiconiaDaemonizer
  # The directory path and the file name of this script
  SCRIPT_DIR    = File.dirname(File.expand_path(__FILE__))
  SCRIPT_NAME   = __FILE__.split('/')[-1]

  class Base
    def self.task_identifier
      @@task_identifier
    end

    def self.uid
      @@uid
    end

    def self.pid_fn(timestamp)
      "/tmp/#{name}.#{timestamp}.pid"
    end
    
    def self.daemonize(task_identifier, uid=nil)
      @@uid = uid || 1000 # The default uid of the daemon process is 10000
      @@task_identifier = task_identifier
      Controller.daemonize(self)
    end  
  end

  module Controller
    
    def self.daemonize(daemon)
      
      if ARGV.include?('--kill-daemon')
        stop(daemon)
      else
        start(daemon)
      end
    end
    
    def self.stop(daemon)
      all_files = Dir.glob(daemon.pid_fn('*')[0..-5])

      pid_found = false
      all_files.each do |pid_file|
	pid, command_id = `cat #{pid_file}`.split("\n")[0..1] # first 2 lines
        if command_id == daemon.task_identifier
          
          `rm #{pid_file}`
          `kill #{pid}`
	  pid_found = true
        end
      end

      if !pid_found
        STDERR.puts "No PID files found for:\n  #{daemon.task_identifier}"
        STDERR.puts
	STDERR.puts "Is the daemon started?"
        STDERR.puts
        exit
      end

    end


    # Daemonizer by Travis Whitton
  
    # Try to fork if at all possible retrying every 5 sec if the
    # maximum process limit for the system has been reached
    def self.safefork
      tryagain = true
  
      while tryagain
        tryagain = false
        begin
          if pid = fork
            return pid
          end
        rescue Errno::EWOULDBLOCK
          sleep 5
          tryagain = true
        end
      end
    end
  
    # This method causes the current running process to become a daemon
    # If closefd is true, all existing file descriptors are closed
    def self.start(daemon, oldmode=0, closefd=false)
  
      srand # Split rand streams between spawning and daemonized process
      safefork and exit # Fork and exit from the parent
  
      pid_file = 
        daemon.pid_fn(Time.now.to_i.to_s + '.' +
                      Process.pid.to_s.rjust(5,'0') + '.' +
                      daemon.task_identifier.rjust(26).gsub(' ', '_')
                     )
  
      raise 'daemon.uid is not set' if daemon.uid == nil
  
      if Process.uid != daemon.uid
        Process.gid = Process.egid =
          `grep -E ^.*?:.*?:#{Process.uid} /etc/passwd`.split(':')[3].to_i
        Process.uid = Process.euid = daemon.uid
      end
  
      # Detach from the controlling terminal
      unless sess_id = Process.setsid
        raise "Cannot detach from controlled terminal"
      end
  
      # Prevent the possibility of acquiring a controlling terminal
      if oldmode.zero?
        trap 'SIGHUP', 'IGNORE'
        exit if pid = safefork
      end
  
  
      `echo #{Process.pid} >> #{pid_file}`
      `echo #{daemon.task_identifier} >> #{pid_file}`
  
  
      Dir.chdir "/"   # Release old working directory
      File.umask 0000 # Insure sensible umask
  
      if closefd
        # Make sure all file descriptors are closed
        ObjectSpace.each_object(IO) do |io|
          unless [STDIN, STDOUT, STDERR].include?(io)
            io.close rescue nil
          end
        end
      end
  
      unless ENV.include?('CICONIA_DEBUG')
        STDIN.reopen "/dev/null"       # Free file descriptors and
        STDOUT.reopen "/dev/null", "a" # point them somewhere sensible
        STDERR.reopen STDOUT           # STDOUT/STDERR should go to a logfile
      end
      
      # The user defined tasks
      daemon.start
      daemon.stop # Clean up after daemon terminates self
                  # if the user defined any cleanup procedures
      
      return # Return value is mostly irrelevant
    end
    
  end
end
