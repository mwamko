#    Mwamko  Copyright (C) 2007  Aleksandr O. Levchuk
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Ansi2Html

  def self.convert (ansi_text)

    @open_tag_count = 0
    ansi_text.gsub!(/\e\[(.*?)m/) do |i|
      
      span_ids = []
      span_tags = ""

      i_flags = $1.split(';')
      i_flags.collect! {|f| f.to_i}
      
      ansi_mapping = {
        
        # style
        1 => 'ansi_bold',
        4 => 'ansi_underline',
        8 => 'ansi_hidden',

        # forground
        30 => 'ansi_black',
        31 => 'ansi_red',
        32 => 'ansi_green',
        33 => 'ansi_yellow',
        34 => 'ansi_blue',
        35 => 'ansi_magenta',
        36 => 'ansi_cyan',
        37 => 'ansi_white',

        # background
        40 => 'ansi_black_bg',
        41 => 'ansi_red_bg',
        42 => 'ansi_green_bg',
        43 => 'ansi_yellow_bg',
        44 => 'ansi_blue_bg',
        45 => 'ansi_magenta_bg',
        46 => 'ansi_cyan_bg',
        47 => 'ansi_white_bg'
      }

      if i_flags.include?(0) or i_flags.empty?
        span_tags << '</span>' * @open_tag_count
        @open_tag_count = 0
      end
      
      ansi_mapping.each do |ansi_code, id|
        span_ids << id if i_flags.include?(ansi_code)
      end

      span_tags << span_ids.collect do |t|
        @open_tag_count = @open_tag_count.next
        "<span id=\"#{t}\">"
      end.join
      
      span_tags
    end

    ansi_text
  end
end
