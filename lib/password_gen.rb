class PasswordGen
  def self.generate_pronounceable(size = 8)
    raise "password size must be an even number" unless size % 2 == 0
    c = %w(1 2 3 4 5 6 7 8 9 0 b c d f g h j k l m n p qu r s t v w x z ch cr fr nd ng nk nt ph pr rd sh sl sp st th tr)
    v = %w(a e i o u y)
    f, r = true, ''
    (size).times do
      r << (f ? c[rand * c.size] : v[rand * v.size])
      f = !f
    end
    r
  end
end
